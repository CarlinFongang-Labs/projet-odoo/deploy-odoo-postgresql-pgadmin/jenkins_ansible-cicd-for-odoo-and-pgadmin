ARG version="3.6-alpine"
FROM python:${version}

LABEL desciption="ic-webapp"
LABEL maintainer="Carlinfg <fongangcarlin@gmail.com>"

WORKDIR /opt

# Installer Flask and git
RUN apk update && \
    apk add --no-cache git bash gawk && \
    pip install Flask

RUN git clone https://github.com/LaboCloud/ic-webapp.git /opt


# Copy the releases.txt file into the working directory
COPY releases.txt /opt/releases.txt
COPY env_vars.sh /opt/env_vars.sh

# Permissions
RUN chmod +x /opt/env_vars.sh && \
    chmod +x env_vars.sh

# Set environment variables
ENV ODOO_URL=$ODOO_URL
ENV PGADMIN_URL=$PGADMIN_URL

# Port
EXPOSE 8080

# Start
ENTRYPOINT ["sh", "-c", "source /opt/env_vars.sh", "-s"]
CMD ["python", "app.py"]
